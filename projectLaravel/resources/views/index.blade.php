<!DOCTYPE html>
<html>
<head>
    <title>Data Mahasiswa</title>
</head>
<body>
    <div >
        <h1 >Daftar Mahasiswa</h1>
        <a href="/addData"> Tambah Data Mahasiswa</a>

        <table>
            <thead >
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIM</th>
                    <th>Action</th>
                <tr>
            </thead>
            <tbody>
            @foreach( $mahasiswa as $mhs)
                <tr>
                    <th >{{$loop->iteration}}</th>
                    <td>{{$mhs->nama}}</td>
                    <td>{{$mhs->nim}}</td>
                    <td>
                        <a href="" class="badge-success">edit</a>
                        <a href="" class="badge-danger">delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>